---
title: " xgensum patch for masterdir"
date: 2024-06-27T00:30:19+05:00
Description: ""
Tags: [voidlinux]
Categories: [Linux, patches]
DisableComments: false
---

# Background
On my voidlinux machine, I sometimes build packages from the source. This machine has 1 HDD and 1 SSD. Due to the slowness of the HDD, I have moved the bootstrap directory to the SSD while the git repository present on the HDD. After updating `srcpkgs/foo/template`’s version, and to get the latest checksums of  the source tarball when I run `xgensum -i foo`, it greets me with an error.
```
...
ERROR: XBPS_CROSS_TRIPLET is not defined!
...
```
Due to the lazyness, I created another bootstrap on HDD, which did trick. But its redundency.

# Solution
When reading through `which xgensum`, it is using `./xbps-src` script to fetch and detect build architecture etc.
Luckily `./xbps-src` has option to explicitly give path to masterdir, So finally after 1 year, I made this patch.


```udiff {linenos=table}
--- xgensum
+++ xgensum     2024-06-27 00:20:45.021081774 +0500
@@ -1,8 +1,8 @@
 #!/bin/bash
-# xgensum [-f] [-c] [-i] [-H hostdir] TEMPLATES ... - update SHA256 sums in templates
+# xgensum [-f] [-c] [-i] [-H hostdir] [-m master-<arch>] TEMPLATES ... - update SHA256 sums in templates
 
 usage() {
-       echo 'Usage: xgensum [-f] [-c] [-i] [-H hostdir] TEMPLATES ...' >&2
+       echo 'Usage: xgensum [-f] [-c] [-i] [-H hostdir] [-m master-<arch>] TEMPLATES ...' >&2
        exit 1
 }
 
@@ -14,16 +14,15 @@
        # pick the first supported arch. This is required for packages unavailable for
        # the host arch
        FLAG_a=
-       if ! "$XBPS_DISTDIR/xbps-src" show-avail "$pkgname" ; then
-               FLAG_a="-a $("$XBPS_DISTDIR/xbps-src" show "$pkgname" | sed -En -e 's/archs:[[:space:]]*([.*]*)/\1/p' | sed -e 's/\*$//' | grep -v -e '^~' | head -n1 )"
+       if ! "$XBPS_DISTDIR/xbps-src" $FLAG_m show-avail "$pkgname" ; then
+               FLAG_a="-a $("$XBPS_DISTDIR/xbps-src" $FLAG_m show "$pkgname" | sed -En -e 's/archs:[[:space:]]*([.*]*)/\1/p' | sed -e 's/\*$//' | grep -v -e '^~' | head -n1 )"
        fi
-
        # Try to source the build-style as well. This is required for R-cran packages.
        if [ -f "${XBPS_DISTDIR}/common/environment/build-style/${build_style}.sh"  ]; then
                . "${XBPS_DISTDIR}/common/environment/build-style/${build_style}.sh"
        fi
 
-       XBPS_SRCDISTDIR=$("$XBPS_DISTDIR/xbps-src" $FLAG_h show-var XBPS_SRCDISTDIR | tail -1)
+       XBPS_SRCDISTDIR=$("$XBPS_DISTDIR/xbps-src" $FLAG_m $FLAG_h show-var XBPS_SRCDISTDIR | tail -1)
        srcdir="$XBPS_SRCDISTDIR/$pkgname-$version"
 
        if [ -n "$FLAG_f" ]; then
@@ -33,10 +32,11 @@
                        distfile="$srcdir/$curfile"
                        rm -vf "$distfile"
                done
-               "$XBPS_DISTDIR/xbps-src" $FLAG_h -I clean $pkgname
+
+               "$XBPS_DISTDIR/xbps-src" $FLAG_m $FLAG_h -I clean $pkgname
        fi
 
-       "$XBPS_DISTDIR/xbps-src" $FLAG_h $FLAG_a -I fetch $pkgname
+       "$XBPS_DISTDIR/xbps-src" $FLAG_m $FLAG_h $FLAG_a -I fetch $pkgname
 
        ret=0
        sums=""
@@ -114,12 +114,13 @@
        return $ret
 }
 
-while getopts fciH:h flag; do
+while getopts fciHm:h flag; do
        case $flag in
                f) FLAG_f=1 ;;
                c) FLAG_c=1 ;;
                i) FLAG_i='-i' ;;
                H) FLAG_h="-H $OPTARG" ;;
+               m) FLAG_m="-m $OPTARG";;
                h|?) usage ;;
        esac
 done
```
